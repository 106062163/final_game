(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/js/home/home.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '19074fcJWRJ+4rKIX6WJxZg', 'home', __filename);
// js/home/home.js

"use strict";

// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        target: 1,
        targetX: 0

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function onLoad() {
        cc.director.preloadScene("Chest", function () {
            cc.log("Next scene preloaded");
        });
    },
    start: function start() {
        var self = this;
        var tmp = this.findObjCord(this.target);
        this.targetX = tmp.x;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        var chData1 = {
            weapon: "staff1",
            health: 1000
        };
        var chData2 = {
            weapon: "bow1",
            health: 1000
        };
        var chData3 = {
            weapon: "staff1",
            health: 1000
        };
        var chData4 = {
            weapon: "sword1",
            health: 1000
        };
        var chData5 = {
            weapon: "sword1",
            health: 1000
        };
        var chData6 = {
            weapon: "shield1",
            health: 1000
        };
        Global.ch = new Array(7);
        Global.ch[1] = chData1;
        Global.ch[2] = chData2;
        Global.ch[3] = chData3;
        Global.ch[4] = chData4;
        Global.ch[5] = chData5;
        Global.ch[6] = chData6;
        cc.log("HI");
    },
    update: function update(dt) {
        /* if (this.node.x != this.targetX) */
        this.MoveCamera(this.target);
    },
    onKeyUp: function onKeyUp(event) {
        if (event.keyCode == cc.macro.KEY.left) {
            if (this.target > 1) {
                this.target--;
            }
        } else if (event.keyCode == cc.macro.KEY.right) {
            if (this.target < 3) {
                this.target++;
            }
        } else if (event.keyCode == cc.macro.KEY.enter) {
            if (this.target == 1) {
                cc.director.loadScene("Chest");
            }
        }
        var tmp = this.findObjCord(this.target);
        this.targetX = tmp.x;
    },
    findObjCord: function findObjCord(t) {
        switch (t) {
            case 1:
                return cc.find("Canvas/obj1");
                break;
            case 2:
                return cc.find("Canvas/obj2");
                break;
            case 3:
                return cc.find("Canvas/obj3");
                break;
            default:

                break;
        }
    },
    MoveCamera: function MoveCamera(t) {
        /* var self = this
        if (self.node.x < this.targetX) {
            self.node.x += 40
        } else if (self.node.x > this.targetX) {
            self.node.x -= 40
        } else {
            var FP = cc.find("Canvas/FocusParticle")
            FP.x = this.targetX
        } */
        var self = this;
        var FP = cc.find("Canvas/FocusParticle");
        if (FP.x < this.targetX) {
            FP.x += 35;
        } else if (FP.x > this.targetX) {
            FP.x -= 35;
        } else {
            FP.x = this.targetX;
        }
    }
});

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=home.js.map
        