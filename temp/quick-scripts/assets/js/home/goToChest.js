(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/js/home/goToChest.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '43093xtujxH+Y04cqcfv68r', 'goToChest', __filename);
// js/home/goToChest.js

"use strict";

// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        particleNode: cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function onLoad() {

        /* cc.director.preloadScene("Chest", function () {
            cc.log("preloaded")
        }); */
        if (Global.ch == null) {
            Global.ch = new Array(7);
            var chData1 = {
                weapon: "staff1",
                health: 1000
            };
            var chData2 = {
                weapon: "bow1",
                health: 1000
            };
            var chData3 = {
                weapon: "staff1",
                health: 1000
            };
            var chData4 = {
                weapon: "sword1",
                health: 1000
            };
            var chData5 = {
                weapon: "sword1",
                health: 1000
            };
            var chData6 = {
                weapon: "shield1",
                health: 1000
            };

            Global.ch[1] = chData1;
            Global.ch[2] = chData2;
            Global.ch[3] = chData3;
            Global.ch[4] = chData4;
            Global.ch[5] = chData5;
            Global.ch[6] = chData6;
        }
    },
    start: function start() {
        var _this = this;

        var Particle = cc.find("Canvas/particle");
        this.node.on(cc.Node.EventType.MOUSE_UP, function (event) {
            cc.director.loadScene("Chest");
        });
        this.node.on(cc.Node.EventType.MOUSE_ENTER, function (event) {
            Particle.x = _this.node.x;
            Particle.y = _this.node.y;
            Particle.active = true;
        });
        this.node.on(cc.Node.EventType.MOUSE_LEAVE, function (event) {
            Particle.active = false;
        });
    },
    update: function update(dt) {}
});

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=goToChest.js.map
        