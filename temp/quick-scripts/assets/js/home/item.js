(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/js/home/item.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '4f5ebnWHMpEX6oewla1vTBm', 'item', __filename);
// js/home/item.js

"use strict";

// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        spriteFrame: {
            default: null,
            type: cc.SpriteFrame
        },
        maxGrid: 49,
        showBoard: {
            default: null,
            type: cc.Layout
        },
        sword1: cc.SpriteFrame,
        sword2: cc.SpriteFrame,
        shield1: cc.SpriteFrame,
        shield2: cc.SpriteFrame,
        bow1: cc.SpriteFrame,
        bow2: cc.SpriteFrame,
        staff1: cc.SpriteFrame,
        staff2: cc.SpriteFrame,
        labelName: cc.Label,
        labelDiscribe: cc.Label,
        usingItemOne: cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function onLoad() {
        var self = this;
        self.showBoard = cc.find("Canvas/ItemProperty");
        self.showBoard.active = false;
    },
    start: function start() {
        var _this = this;

        var self = this;
        var gearArray;
        if (Global.displayHigh == false) {
            gearArray = ["sword1", "bow1", "staff1", "shield1"];
        } else {
            gearArray = ["sword1", "sword2", "bow1", "bow2", "staff1", "staff2", "shield1", "shield2"];
        }

        /* var datajj = JSON.parse(cc.sys.localStorage.getItem('backpack')) */
        var datajj = gearArray;
        for (var i = 0; i < this.maxGrid; i++) {
            var node = new cc.Node("NODE");
            var sprite = node.addComponent(cc.Sprite);
            if (datajj[i]) this.judgePlacement(datajj[i], node, sprite);else this.judgePlacement("box", node, sprite);
            node.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                _this.showBoard.active = true;
                self.showBoard.x = event._x - 800;
                self.showBoard.y = event._y - 369;
            });
            node.on(cc.Node.EventType.MOUSE_LEAVE, function (event) {
                _this.showBoard.active = false;
            });

            node.opacity = 255;
            self.node.addChild(node);
        }
    },
    update: function update(dt) {},
    judgePlacement: function judgePlacement(gear, node, sprite) {
        var _this2 = this;

        /* var sprite = node.addComponent(cc.Sprite);
        sprite.spriteFrame = this.spriteFrame */
        if (gear == "box") {
            sprite.spriteFrame = this.spriteFrame;
            node.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                _this2.labelName.string = "空格";
                _this2.labelDiscribe.string = "";
            });
        } else {
            if (gear == "sword1") {
                sprite.spriteFrame = this.sword1;
                node.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                    _this2.labelName.string = "新手の劍";
                    _this2.labelDiscribe.string = "新手才會用的劍";
                });
                node.rotation = 45;
            } else if (gear == "sword2") {
                sprite.spriteFrame = this.sword2;
                node.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                    _this2.labelName.string = "強者の劍";
                    _this2.labelDiscribe.string = "強者才會用的劍";
                });
                node.rotation = 45;
            } else if (gear == "bow1") {
                sprite.spriteFrame = this.bow1;
                node.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                    _this2.labelName.string = "新手の弓";
                    _this2.labelDiscribe.string = "新手才會用的弓";
                });
            } else if (gear == "bow2") {
                sprite.spriteFrame = this.bow2;
                node.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                    _this2.labelName.string = "強者の弓";
                    _this2.labelDiscribe.string = "強者才會用的弓";
                });
            } else if (gear == "staff1") {
                sprite.spriteFrame = this.staff1;
                node.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                    _this2.labelName.string = "新手の杖";
                    _this2.labelDiscribe.string = "新手才會用的杖";
                });
            } else if (gear == "staff2") {
                sprite.spriteFrame = this.staff2;
                node.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                    _this2.labelName.string = "強者の魔杖";
                    _this2.labelDiscribe.string = "強者才會用的魔杖";
                });
            } else if (gear == "shield1") {
                sprite.spriteFrame = this.shield1;
                node.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                    _this2.labelName.string = "新手の盾";
                    _this2.labelDiscribe.string = "新手才會用的盾";
                });
            } else if (gear == "shield2") {
                sprite.spriteFrame = this.shield2;
                node.on(cc.Node.EventType.MOUSE_MOVE, function (event) {
                    _this2.labelName.string = "強者の盾";
                    _this2.labelDiscribe.string = "強者才會用的盾";
                });
            }
            node.width = 51;
            node.height = 50;
            sprite.width = 51;
            sprite.height = 50;
            node.on(cc.Node.EventType.MOUSE_UP, function (event) {
                var nodeComp = _this2.usingItemOne.getComponent("usingItem");

                nodeComp.changeUsingItem(gear);
            });
        }
    }
});

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=item.js.map
        