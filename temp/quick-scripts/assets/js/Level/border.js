(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/js/Level/border.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '24194zWEwJO37WaH2munLfx', 'border', __filename);
// js/Level/border.js

"use strict";

// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        target: 1,
        Level1_node: {
            default: null,
            type: cc.Node
        },
        Level2_node: {
            default: null,
            type: cc.Node
        },
        Level3_node: {
            default: null,
            type: cc.Node
        },
        Level4_node: {
            default: null,
            type: cc.Node
        },
        Level5_node: {
            default: null,
            type: cc.Node
        }
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start: function start() {
        var _this = this;

        this.Level1_node.on(cc.Node.EventType.MOUSE_ENTER, function (event) {
            _this.borderMove(_this.Level1_node);
        });
        this.Level1_node.on(cc.Node.EventType.MOUSE_LEAVE, function (event) {
            document.body.style.cursor = "default";
        });
        this.Level2_node.on(cc.Node.EventType.MOUSE_ENTER, function (event) {
            _this.borderMove(_this.Level2_node);
        });
        this.Level2_node.on(cc.Node.EventType.MOUSE_LEAVE, function (event) {
            document.body.style.cursor = "default";
        });
        this.Level3_node.on(cc.Node.EventType.MOUSE_ENTER, function (event) {
            _this.borderMove(_this.Level3_node);
        });
        this.Level3_node.on(cc.Node.EventType.MOUSE_LEAVE, function (event) {
            document.body.style.cursor = "default";
        });
        this.Level4_node.on(cc.Node.EventType.MOUSE_ENTER, function (event) {
            _this.borderMove(_this.Level4_node);
        });
        this.Level4_node.on(cc.Node.EventType.MOUSE_LEAVE, function (event) {
            document.body.style.cursor = "default";
        });
        this.Level5_node.on(cc.Node.EventType.MOUSE_ENTER, function (event) {
            _this.borderMove(_this.Level5_node);
        });
        this.Level5_node.on(cc.Node.EventType.MOUSE_LEAVE, function (event) {
            document.body.style.cursor = "default";
        });
    },
    update: function update(dt) {},
    borderMove: function borderMove(targetNode) {
        document.body.style.cursor = "pointer";
        this.node.y = targetNode.y;
    }
});

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=border.js.map
        