"use strict";
cc._RF.push(module, '4c267jJwg1JiqBLwr/L9u01', 'chest');
// js/home/chest.js

"use strict";

// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        leftChange: cc.Node,
        rightChange: cc.Node,
        displayCH: 1,
        ChLabel: cc.Node,
        ChList: Array,
        usingItem1: cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function onLoad() {
        cc.director.preloadScene("Home");
        if (Global.ch == null) {
            Global.ch = new Array(7);
            var chData1 = {
                weapon: "staff1",
                health: 1000
            };
            var chData2 = {
                weapon: "bow1",
                health: 1000
            };
            var chData3 = {
                weapon: "staff1",
                health: 1000
            };
            var chData4 = {
                weapon: "sword1",
                health: 1000
            };
            var chData5 = {
                weapon: "sword1",
                health: 1000
            };
            var chData6 = {
                weapon: "shield1",
                health: 1000
            };

            Global.ch[1] = chData1;
            Global.ch[2] = chData2;
            Global.ch[3] = chData3;
            Global.ch[4] = chData4;
            Global.ch[5] = chData5;
            Global.ch[6] = chData6;
        }
    },
    start: function start() {
        var _this = this;

        var chData = {
            weapon: ""
        };
        this.ChList = new Array(7);

        this.updateChList();
        this.displayCH = 1;
        this.displayCurrentCH();
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, function (key) {
            if (key.keyCode == cc.macro.KEY.backspace) {
                cc.director.loadScene("Home");
            }
        }, this);
        this.leftChange.on(cc.Node.EventType.MOUSE_UP, function (event) {
            if (_this.displayCH > 1) {
                _this.displayCH--;
                _this.displayCurrentCH();
            }
        });
        this.rightChange.on(cc.Node.EventType.MOUSE_UP, function (event) {
            if (_this.displayCH < 6) {
                _this.displayCH++;
                _this.displayCurrentCH();
            }
        });
    },
    update: function update(dt) {},
    displayCurrentCH: function displayCurrentCH() {

        this.updateChList();

        this.hideAllCh();
        var stringLabel = this.ChLabel.getComponent(cc.Label);
        stringLabel.string = "角色" + this.displayCH;
        var itemOneSprite = this.usingItem1.getComponent(cc.Sprite);
        var itemScript = this.usingItem1.getComponent('usingItem');
        if (this.ChList[this.displayCH] == "sword1" || this.ChList[this.displayCH] == "sword2") {
            var ch3 = cc.find('Canvas/Character/knight_1');
            ch3.active = true;
            var wp = cc.find('Canvas/Character/knight_1/weapon');
            var wpChange = wp.getComponent('changeWeapon');
            if (this.ChList[this.displayCH] == "sword1") {
                itemOneSprite.spriteFrame = itemScript.sword1;
                wpChange.weaponChange(1);
            } else {
                itemOneSprite.spriteFrame = itemScript.sword2;
                wpChange.weaponChange(2);
            }
        } else if (this.ChList[this.displayCH] == "bow1" || this.ChList[this.displayCH] == "bow2") {
            var ch1 = cc.find('Canvas/Character/anchor');
            ch1.active = true;
            var _wp = cc.find('Canvas/Character/anchor/row_1');
            var _wpChange = _wp.getComponent('changeWeapon');
            if (this.ChList[this.displayCH] == "bow1") {
                itemOneSprite.spriteFrame = itemScript.bow1;
                _wpChange.weaponChange(1);
            } else {
                itemOneSprite.spriteFrame = itemScript.bow2;
                _wpChange.weaponChange(2);
            }
        } else if (this.ChList[this.displayCH] == "staff1" || this.ChList[this.displayCH] == "staff2") {
            var ch2 = cc.find('Canvas/Character/magic');
            ch2.active = true;
            var _wp2 = cc.find('Canvas/Character/magic/staff_1');
            var _wpChange2 = _wp2.getComponent('changeWeapon');
            if (this.ChList[this.displayCH] == "staff1") {
                itemOneSprite.spriteFrame = itemScript.staff1;
                _wpChange2.weaponChange(1);
            } else {
                itemOneSprite.spriteFrame = itemScript.staff2;
                _wpChange2.weaponChange(2);
            }
        } else if (this.ChList[this.displayCH] == "shield1" || this.ChList[this.displayCH] == "shield2") {
            var ch4 = cc.find('Canvas/Character/knight_2');
            ch4.active = true;
            var _wp3 = cc.find('Canvas/Character/knight_2/shield_1');
            var _wpChange3 = _wp3.getComponent('changeWeapon');
            if (this.ChList[this.displayCH] == "shield1") {
                itemOneSprite.spriteFrame = itemScript.shield1;
                _wpChange3.weaponChange(1);
            } else {
                itemOneSprite.spriteFrame = itemScript.shield2;
                _wpChange3.weaponChange(2);
            }
        }
    },
    hideAllCh: function hideAllCh() {
        var ch1 = cc.find('Canvas/Character/anchor');
        var ch2 = cc.find('Canvas/Character/magic');
        var ch3 = cc.find('Canvas/Character/knight_1');
        var ch4 = cc.find('Canvas/Character/knight_2');
        ch1.active = false;
        ch2.active = false;
        ch3.active = false;
        ch4.active = false;
    },
    updateChList: function updateChList() {

        /* let c1 = JSON.parse(cc.sys.localStorage.getItem('ch1'));
        let c2 = JSON.parse(cc.sys.localStorage.getItem('ch2'));
        let c3 = JSON.parse(cc.sys.localStorage.getItem('ch3'));
        let c4 = JSON.parse(cc.sys.localStorage.getItem('ch4'));
        let c5 = JSON.parse(cc.sys.localStorage.getItem('ch5'));
        let c6 = JSON.parse(cc.sys.localStorage.getItem('ch6')); */

        this.ChList = ["", Global.ch[1].weapon, Global.ch[2].weapon, Global.ch[3].weapon, Global.ch[4].weapon, Global.ch[5].weapon, Global.ch[6].weapon];
    }
});

cc._RF.pop();