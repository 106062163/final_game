"use strict";
cc._RF.push(module, '97df6eDtvFL3KTZ5HLhAjVf', 'rainbow');
// export/music/scripts/rainbow.js

'use strict';

cc.Class({
    extends: cc.Component,

    properties: {
        thebit: {
            default: null,
            type: cc.AudioClip
        },

        pata: {
            default: null,
            type: cc.AudioClip
        },

        pon: {
            default: null,
            type: cc.AudioClip
        },

        don: {
            default: null,
            type: cc.AudioClip
        },

        chaka: {
            default: null,
            type: cc.AudioClip
        },

        MissSound: {
            default: null,
            type: cc.AudioClip
        },

        feversound: {
            default: null,
            type: cc.AudioClip
        },

        bgmusic: {
            default: null,
            type: cc.AudioClip
        },

        combostring: {
            default: null,
            type: cc.RichText
        },
        marks: 0, //評價每個完整指令有幾分 output比人用
        state: 0

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function onLoad() {
        this.over = 1;
        this.sss = 1; //開始時count beat
        this.tap = 0; //你每次一按，距離準確節拍的時間
        this.miss = 0; //miss判斷
        this.missed = 0;
        this.delay = 0.5; //節拍間距
        this.timenow = 0; //現在的系統時間
        this.nexttime = 0; //下個指令的時間(要+delay用才是下個指令)
        this.timer = 0; //用fps計的時間
        this.thebgm = 0; //bgm
        this.instru = new Array(4); //指令的buffer
        this.instruidx = 0; //指令的index
        this.startmusic = 0; //判斷是否在播音樂前
        this.cantap = false; //每次input完4個指令，後4拍都不能input
        this.notapcount = 0; //不能按節拍的counter
        this.fevermark = 0; //目前fever的分數

        this.combo = 0;
        this.instrudisplay = 0;
        this.combonode = cc.find('Canvas/Main Camera/intruimg/combo');
        this.bar = cc.find('Canvas/Main Camera/intruimg/progressbar_2/bar_2');
        this.progressbar = cc.find('Canvas/Main Camera/intruimg/progressbar_2');
        this.full = false;
        this.opacitynochange = 0;

        this.count = 0;
        this.ranstate = 0;

        this.counttimeear = new Date().getMilliseconds(); //節拍與按時間的counter

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    },


    checkdetect: function checkdetect() {
        this.timer = 0;
        this.miss = 0;
        this.missed = 0;

        //計算時間差
        this.nowtime = new Date().getMilliseconds();
        if (this.nowtime >= 500) this.nowtime -= 500;
        this.tap = Math.abs(this.nowtime - this.counttimeear);
        //console.log(this.tap);


        //分數判定
        if (this.tap >= 450 && this.tap <= 500 || this.tap >= 0 && this.tap <= 50) {
            this.marks += 0.25;
        } else if (this.tap >= 300 && this.tap <= 449 || this.tap >= 51 && this.tap <= 200) {
            this.marks += 0.2;
        } else if (this.tap >= 250 && this.tap <= 299 || this.tap >= 201 && this.tap <= 249) {
            this.marks += 0.1;
        }

        console.log(this.marks);

        //檢查指令是否正確
        if (this.instruidx < 3) this.instruidx++;else {
            this.instruidx = 0;
            this.cantap = false;
            this.combo++;
            this.combostring.string = '<outline color=#000000 width=3>Combo ' + this.combo + '</outline>';

            //fever判斷
            this.fevermark += this.marks;
            console.log(this.fevermark);
            if (this.fevermark >= 3) {
                this.marks = 1.2;
                cc.audioEngine.playEffect(this.feversound, false);
                this.bar.runAction(cc.scaleTo(1, 0, 1));
            } else {
                this.bar.runAction(cc.scaleTo(1, 1 - this.fevermark / 3, 1));
            }

            if (this.instru.join('') == 'sssk') this.state = 1; //console.log('前進')
            else if (this.instru.join('') == 'kksk') this.state = 2; //console.log('攻擊')
                else if (this.instru.join('') == 'aask') this.state = 3; //console.log('防禦')
                    else if (this.instru.join('') == 'lsls') this.state = 4; //console.log('迴避')
                        else {
                                console.log('無效指令');
                                cc.audioEngine.playEffect(this.MissSound, false);
                                this.bar.runAction(cc.scaleTo(1, 1, 1));
                                this.combo = 0;
                                this.fevermark = 0;
                                this.combostring.string = '<outline color=#000000 width=3>Combo ' + this.combo + '</outline>';
                            }
            var canv = cc.find("Canvas");
            var sc = canv.getComponent("GameManager");
            sc.signalCome(this.marks, this.state);
            this.marks = 0;
            this.state = 0;
        }
    },

    onKeyDown: function onKeyDown(event) {
        if (this.cantap) {

            switch (event.keyCode) {
                case cc.macro.KEY.a:
                    cc.audioEngine.playEffect(this.chaka, false);
                    this.instru[this.instruidx] = 'a';
                    this.instrudis('chaka');
                    this.checkdetect();
                    break;

                case cc.macro.KEY.s:
                    cc.audioEngine.playEffect(this.pata, false);
                    this.instru[this.instruidx] = 's';
                    this.instrudis('pata');
                    this.checkdetect();
                    break;

                case cc.macro.KEY.k:
                    cc.audioEngine.playEffect(this.pon, false);
                    this.instru[this.instruidx] = 'k';
                    this.instrudis('pon');
                    this.checkdetect();
                    break;

                case cc.macro.KEY.l:
                    cc.audioEngine.playEffect(this.don, false);
                    this.instru[this.instruidx] = 'l';
                    this.instrudis('don');
                    this.checkdetect();
                    break;
            }
            this.notapcount = 1;
        }
    },

    instrudis: function instrudis(theinstru) {
        console.log(theinstru);
        this.instrudisplay = cc.find('Canvas/Main Camera/intruimg/' + theinstru);

        //this.node.getChildByName(theinstru);

        if (theinstru == 'chaka') {
            this.instrudisplay.position = cc.v2(Math.random() * 600 - 300, 250);
            this.instrudisplay.setRotation(Math.random() * 100 - 50);
        } else if (theinstru == 'pata') {
            this.instrudisplay.position = cc.v2(-680, Math.random() * 400 - 200);
            this.instrudisplay.setRotation(Math.random() * 50);
        } else if (theinstru == 'pon') {
            this.instrudisplay.position = cc.v2(680, Math.random() * 400 - 200);
            this.instrudisplay.setRotation(Math.random() * 100 - 50);
        } else if (theinstru == 'don') {
            this.instrudisplay.position = cc.v2(Math.random() * 600 - 300, -250);
            this.instrudisplay.setRotation(Math.random() * 100 - 50);
        }

        this.instrudisplay.runAction(cc.sequence(cc.scaleTo(0.15, 1), cc.scaleTo(0.15, 0.5)));
        this.instrudisplay.runAction(cc.sequence(cc.fadeIn(0.15, 1), cc.fadeOut(0.15, 0)));
    },

    beatdisplay: function beatdisplay(dt) {
        if (this.fevermark < 3) this.node.color = new cc.Color(120, 120, 120);else this.node.color = new cc.Color(255, 255, 255);
    },

    beatsound: function beatsound(dt) {
        if (this.timer >= 0.49 && this.sss <= 5) {
            this.node.runAction(cc.sequence(cc.fadeTo(0.1, 150), cc.fadeTo(0.1, 0)));
            if (this.sss == 5) {
                cc.audioEngine.playEffect(this.thebit, false);
                this.thebgm = cc.audioEngine.play(this.bgmusic, true, 0.7);
                this.timenow = cc.audioEngine.getCurrentTime(this.thebgm);
                this.startmusic = 1;
                this.count++;
            } else if (this.sss == 4) {
                this.cantap = true;
                cc.audioEngine.playEffect(this.thebit, false);
            } else {
                cc.audioEngine.playEffect(this.thebit, false);
            }
            this.timer = 0;
            this.sss++;
        } else {
            this.timenow = cc.audioEngine.getCurrentTime(this.thebgm);
            if (this.timenow <= 0.2) this.nexttime = 0; //重複音樂時reset計時器
        }

        if (this.nexttime + this.delay <= this.timenow && this.startmusic) {
            //console.log(this.cantap);
            this.beforetime = this.nexttime;
            this.nexttime = this.nexttime + this.delay;
            //console.log(this.nexttime);
            cc.audioEngine.playEffect(this.thebit, false);
            this.counttimeear = new Date().getMilliseconds();
            if (this.counttimeear >= 500) this.counttimeear -= 500;

            if (this.miss && this.cantap) {
                this.missed++;
                if (this.missed == 1) cc.audioEngine.playEffect(this.MissSound, false);
                this.instruidx = 0;
                this.marks = 0;
                this.fevermark = 0;
                this.combo = 0;
                this.combostring.string = '<outline color=#000000 width=3>Combo ' + this.combo + '</outline>';
                this.bar.runAction(cc.scaleTo(1, 1, 1));
                console.log('miss');
            } else this.miss = 1;

            if (this.notapcount == 5) {
                this.notapcount++;
                this.cantap = true;
                this.miss = 0;
            }
            if (!this.cantap) this.notapcount++;

            this.opacitynochange = 0;

            if (this.fevermark >= 3) {
                this.progressbar.runAction(cc.sequence(cc.scaleTo(0.07, 0.45, 0.4), cc.scaleTo(0.07, 0.4, 0.4)));
            }

            this.combonode.runAction(cc.sequence(cc.scaleTo(0.1, 1, 1.3), cc.scaleTo(0.1, 1, 1)));
            this.node.runAction(cc.sequence(cc.fadeTo(0.1, 150), cc.fadeTo(0.1, 0)));

            this.count++;
            if (this.count >= 4) {
                var canv = cc.find("Canvas");
                var sc = canv.getComponent("GameManager");

                if (this.ranstate == 1) {
                    //todo
                    sc.readyAttack();
                    this.ranstate = 2;
                } else if (this.ranstate == 2) {
                    //todo
                    sc.monsterAttack();
                    this.ranstate = Math.floor(Math.random() * Math.floor(2));
                } else this.ranstate = Math.floor(Math.random() * Math.floor(2));
                this.count = 0;
            }

            //console.log(state);
        }
    },

    update: function update(dt) {
        this.timer += dt;
        this.beatsound(dt);
        this.beatdisplay(dt);
    }
});

cc._RF.pop();