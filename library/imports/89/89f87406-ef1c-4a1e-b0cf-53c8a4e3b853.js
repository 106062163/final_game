"use strict";
cc._RF.push(module, '89f87QG7xxKHrDPU8ik47hT', 'usingItem');
// js/Chest/usingItem.js

'use strict';

// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        sword1: cc.SpriteFrame,
        sword2: cc.SpriteFrame,
        shield1: cc.SpriteFrame,
        shield2: cc.SpriteFrame,
        bow1: cc.SpriteFrame,
        bow2: cc.SpriteFrame,
        staff1: cc.SpriteFrame,
        staff2: cc.SpriteFrame
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start: function start() {
        /* this.changeUsingItem("bow1") */

    },
    changeUsingItem: function changeUsingItem(itemName) {
        var sprite = this.getComponent(cc.Sprite);
        var currentCh = cc.find('Canvas');
        var ChestScript = currentCh.getComponent('chest');
        var chData = {
            weapon: "",
            health: 1000
        };

        if (itemName == "sword1") {
            sprite.spriteFrame = this.sword1;
            chData.weapon = "sword1";
        } else if (itemName == "sword2") {
            sprite.spriteFrame = this.sword2;
            chData.weapon = "sword2";
        } else if (itemName == "shield1") {
            sprite.spriteFrame = this.shield1;
            chData.weapon = "shield1";
        } else if (itemName == "shield2") {
            sprite.spriteFrame = this.shield2;
            chData.weapon = "shield2";
        } else if (itemName == "bow1") {
            sprite.spriteFrame = this.bow1;
            chData.weapon = "bow1";
        } else if (itemName == "bow2") {
            sprite.spriteFrame = this.bow2;
            chData.weapon = "bow2";
        } else if (itemName == "staff1") {
            sprite.spriteFrame = this.staff1;
            chData.weapon = "staff1";
        } else if (itemName == "staff2") {
            sprite.spriteFrame = this.staff2;
            chData.weapon = "staff2";
        }
        /* cc.sys.localStorage.setItem('ch' + ChestScript.displayCH, JSON.stringify(chData)); */

        Global.ch[ChestScript.displayCH] = chData;

        ChestScript.displayCurrentCH();
        this.node.width = 40;
        this.node.height = 40;
    }
    // update (dt) {},

});

cc._RF.pop();