// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },

        anchor: cc.Prefab,
        magic: cc.Prefab,
        knight1: cc.Prefab,
        knight2: cc.Prefab,
        chArray: Array,
        canvas: cc.Canvas,
        mainCamera: cc.Node,
        chList: Array,
        enemy: Array,
        arrow: cc.Prefab,
        firemagic: cc.Prefab,
        Level: {
            default: 1,
            type: cc.Integer
        },
        damage: {
            default: 0,
            type: cc.Integer
        },
        attack1: cc.Node,
        attack2: cc.Node,
        attack3: cc.Node,
        k1Die: cc.SpriteFrame,
        k2Die: cc.SpriteFrame,
        anchorDie: cc.SpriteFrame,
        magicDie: cc.SpriteFrame,
        lose: cc.Node,
        win: cc.Node,
        hpBars: Array,
        dies: 0,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        this.attack1.active = false
        this.attack2.active = false
        this.attack3.active = false
        this.lose.active = false
        this.win.active = false
        cc.director.getCollisionManager().enabled = true;
        this.chArray = new Array(7)
        this.chList = new Array(7)
        this.enemy = new Array(3)
        this.hpBars = new Array(7)
        this.enemy[0] = cc.find('Canvas/enemy')
        this.enemy[1] = cc.find('Canvas/enemy2')
        this.enemy[2] = cc.find('Canvas/boss')
        this.enemy[0].alive = true
        this.enemy[1].alive = true
        this.enemy[2].alive = true
        this.enemy[0].health = this.Level * 1000
        this.enemy[1].health = this.Level * 2000
        this.enemy[2].health = this.Level * 3000


        this.canvas = cc.find('Canvas')
        var i
        for (i = 1; i <= 6; i++) {
            this.chArray[i] = this.checkChWeapon(i)

            if (this.chArray[i] != null) {
                this.canvas.addChild(this.chArray[i])
                this.chArray[i].position = cc.v2(-700 + (i - 1) * 100, -200);
                this.chArray[i].scale = 0.35
                this.chArray[i].alive = true
            }

        }

        this.moveAction()
        this.changeAnimation(0)
    },
    checkChWeapon(num) {
        var chData = Global.ch[num]
        /* var chData = JSON.parse(cc.sys.localStorage.getItem('ch' + num)) */
        switch (chData.weapon) {
            case "sword1":
                this.damage += 60
                var n = cc.instantiate(this.knight1)
                var wp = n.getChildByName("weapon");
                var sc = wp.getComponent('changeWeapon')
                sc.weaponChange(1)
                this.chList[num] = "knight11"
                n.health = chData.health
                return n
                break;
            case "sword2":
                this.damage += 150
                var n = cc.instantiate(this.knight1)
                var wp = n.getChildByName("weapon");
                var sc = wp.getComponent('changeWeapon')
                sc.weaponChange(2)
                this.chList[num] = "knight12"
                n.health = chData.health
                return n
                break;
            case "bow1":
                this.damage += 50
                var n = cc.instantiate(this.anchor)
                var wp = n.getChildByName("row_1");
                var sc = wp.getComponent('changeWeapon')
                sc.weaponChange(1)
                this.chList[num] = "anchor1"
                n.health = chData.health
                return n
                break;
            case "bow2":
                this.damage += 120
                var n = cc.instantiate(this.anchor)
                var wp = n.getChildByName("row_1");
                var sc = wp.getComponent('changeWeapon')
                sc.weaponChange(2)
                this.chList[num] = "anchor2"
                n.health = chData.health
                return n
                break;
            case "staff1":
                this.damage += 80
                var n = cc.instantiate(this.magic)
                var wp = n.getChildByName("staff_1");
                var sc = wp.getComponent('changeWeapon')
                sc.weaponChange(1)
                this.chList[num] = "magic1"
                n.health = chData.health
                return n
                break;
            case "staff2":
                this.damage += 170
                var n = cc.instantiate(this.magic)
                var wp = n.getChildByName("staff_1");
                var sc = wp.getComponent('changeWeapon')
                sc.weaponChange(2)
                this.chList[num] = "magic2"
                n.health = chData.health
                return n
                break;
            case "shield1":
                this.damage += 20
                var n = cc.instantiate(this.knight2)
                var wp = n.getChildByName("shield_1");
                var sc = wp.getComponent('changeWeapon')
                sc.weaponChange(1)
                this.chList[num] = "knight21"
                n.health = chData.health + 400
                return n
                break;
            case "shield2":
                this.damage += 80
                var n = cc.instantiate(this.knight2)
                var wp = n.getChildByName("shield_1");
                var sc = wp.getComponent('changeWeapon')
                sc.weaponChange(2)
                this.chList[num] = "knight22"
                n.health = chData.health + 800
                return n
                break;
            default:
                return null
                break;
        }
    },
    changeAnimation(num) {
        var i
        for (i = 1; i <= 6; i++) {
            if (this.chArray[i].alive) {
                var anim = this.chArray[i].getComponent(cc.Animation);

                var animState = anim.play(anim._clips[num].name);
                animState.wrapMode = cc.WrapMode.Loop;

            }

        }

    },
    update(dt) {
        if (this.attack1.active) {
            this.attack1.x -= 10
        }
        if (this.attack3.active) {
            let anim = this.attack3.getComponent(cc.Animation);
            cc.log(anim)
            let animState
            if (this.Level == 2) animState = anim.getAnimationState('attack2');
            else animState = anim.getAnimationState('attack3');
            cc.log(animState)
            if (!animState.isPlaying) {

                this.attack3.active = false
            }
        }

        let hp1 = cc.find("Canvas/hp1")
        let hp2 = cc.find("Canvas/hp2")
        let hp3 = cc.find("Canvas/hp3")
        let hp4 = cc.find("Canvas/hp4")
        let hp5 = cc.find("Canvas/hp5")
        let hp6 = cc.find("Canvas/hp6")
        let hps = ["", hp1, hp2, hp3, hp4, hp5, hp6]

        for (let i = 1; i <= 6; i++) {

            hps[i].position = cc.v2(this.chArray[i].x, this.chArray[i].y + 60)
            if (this.chArray[i].health > 0)
                hps[i].width = 80 * (this.chArray[i].health / 1000)
            else
                hps[i].width = 0
            if (this.chArray[i].alive) {
                if (this.chArray[i].health <= 0) {
                    this.dies++
                    if (this.dies == 6) {
                        let rainbow = cc.find("Canvas/Main Camera/rainbow")
                        let sc = rainbow.getComponent("rainbow")

                        cc.audioEngine.stopAll();
                        this.lose.active = true
                    }
                    this.detectAndDie(i)
                }
            }
        }

        if (this.enemy[2].active == false) {
            this.win.active = true
            Global.displayHigh = true
            cc.audioEngine.stopAll()
            /*  if (this.Level == 1) {
                 var datajj = JSON.parse(cc.sys.localStorage.getItem('backpack'))
                 datajj.push("sword2")
                 datajj.push("bow2")
                 datajj.push("staff2")
                 datajj.push("shield2")
                 cc.log(datajj)
                 cc.sys.localStorage.setItem('backpack', JSON.stringify(datajj));
             } */
        }


    },
    signalCome(mark, state) {

        switch (state) {
            case 1:

                if (this.enemy[0].alive) {
                    for (let a = 1; a <= 6; a++) {
                        if (this.chArray[a].x <= this.enemy[0].x - 200 && this.chArray[a].alive) {
                            this.moveAction()
                            break
                        }
                    }
                } else if (this.enemy[1].alive) {
                    for (let a = 1; a <= 6; a++) {
                        if (this.chArray[a].x <= this.enemy[1].x - 200 && this.chArray[a].alive) {
                            this.moveAction()
                            break
                        }
                    }
                } else if (this.enemy[2].alive) {
                    for (let a = 1; a <= 6; a++) {
                        if (this.chArray[a].x <= this.enemy[2].x - 200 && this.chArray[a].alive) {
                            this.moveAction()
                            break
                        }
                    }
                }
                break;
            case 2:
                let tmpX
                for (let k = 6; k >= 1; k--) {
                    if (this.chArray[k].alive) {
                        tmpX = this.chArray[k].x
                        break;
                    }
                }
                if (this.enemy[0].alive) {
                    if (tmpX >= this.enemy[0].x - 300)
                        this.attackAction(this.enemy[0], mark)

                } else if (this.enemy[1].alive) {
                    if (tmpX >= this.enemy[1].x - 300)
                        this.attackAction(this.enemy[1], mark)
                } else if (this.enemy[2].alive) {
                    if (tmpX >= this.enemy[2].x - 300)
                        this.attackAction(this.enemy[2], mark)
                }
                break;
            case 3:

                break;
            case 4:
                this.RunAway();
                break;
            default:
                break;
        }
    },
    moveAction() {
        var i
        for (i = 1; i <= 6; i++) {
            if (this.chArray[i].alive)
                this.chArray[i].runAction(cc.moveBy(2, 160, 0))
        }
        cc.tween(this.mainCamera).to(2, {
            x: this.mainCamera.x + 160
        }).start()

    },
    attackAction(em, mark) {
        for (let i = 1; i <= 6; i++) {
            if (this.chArray[i].alive) {
                let anim = this.chArray[i].getComponent(cc.Animation);
                let dx = em.x - this.chArray[i].x;
                /*  cc.log(anim) */
                let jumpY = Math.random() * 200 + 200
                let jumpTime = Math.random() * 0.35 + 0.3

                switch (this.chList[i]) {
                    case "knight11":
                        let finished1 = cc.callFunc(() => {
                            anim.play('n_knight_attack');
                            anim.on('finished', () => {
                                if (em) {
                                    em.health -= 50 * mark
                                    if (em.health <= 0) {
                                        em.alive = false
                                        em.active = false
                                    }
                                }
                                this.chArray[i].runAction(
                                    cc.moveBy(0.5 + i * 0.15, -dx, 0)
                                );
                                anim.play('n_knight_1')
                                anim.off('finished');
                            });
                        });
                        let Action1 = cc.sequence(cc.moveBy(0.5 + i * 0.15, dx, 0), finished1);
                        this.chArray[i].runAction(Action1);

                        break;
                    case "knight12":
                        let finished2 = cc.callFunc(() => {
                            anim.play('n_knight_attack');
                            anim.on('finished', () => {
                                if (em) {
                                    em.health -= 150 * mark
                                    if (em.health <= 0) {
                                        em.alive = false
                                        em.active = false
                                    }
                                }
                                this.chArray[i].runAction(
                                    cc.moveBy(0.5 + i * 0.15, -dx, 0)
                                );
                                anim.play('n_knight_1')
                                anim.off('finished');
                            });
                        });
                        let Action = cc.sequence(cc.moveBy(0.5 + i * 0.15, dx, 0), finished2);
                        this.chArray[i].runAction(Action);
                        break;

                    case "knight21":
                        let Action3 = cc.sequence(cc.moveBy(0.1 + i * 0.1, dx, 0), cc.callFunc(() => {
                            if (em) {
                                em.health -= 20 * mark
                                if (em.health <= 0) {
                                    em.alive = false
                                    em.active = false
                                }
                            }
                        }), cc.moveBy(0.1 + i * 0.1, -dx, 0));
                        this.chArray[i].runAction(Action3);
                        break;

                    case "knight22":
                        let Action4 = cc.sequence(cc.moveBy(0.1 + i * 0.1, dx, 0), cc.callFunc(() => {
                            if (em) {
                                em.health -= 60 * mark
                                if (em.health <= 0) {
                                    em.alive = false
                                    em.active = false
                                }
                            }
                        }), cc.moveBy(0.1 + i * 0.1, -dx, 0));
                        this.chArray[i].runAction(Action4);
                        break;

                    case "anchor1":
                        let finished5 = cc.callFunc(() => {
                            anim.play('anchor_attack');
                            anim.on('finished', () => {
                                let arrow = cc.instantiate(this.arrow)
                                this.canvas.addChild(arrow)

                                arrow.position = cc.v2(this.chArray[i].x, this.chArray[i].y);
                                let Action55 = cc.sequence(cc.moveBy(0.5, em.x - arrow.x, em.y - arrow.y), cc.callFunc(() => {
                                    if (em) {
                                        em.health -= 70 * mark
                                        if (em.health <= 0) {
                                            em.alive = false
                                            em.active = false
                                        }
                                    }
                                    arrow.destroy()
                                }));
                                arrow.runAction(Action55);
                                this.chArray[i].runAction(
                                    cc.moveBy(jumpTime, 0, -jumpY)
                                );
                                anim.play('anchor')
                                anim.off('finished');
                            });
                        });
                        let Action5 = cc.sequence(cc.moveBy(jumpTime, 0, jumpY), finished5);
                        this.chArray[i].runAction(Action5);
                        break
                    case "anchor2":
                        let finished6 = cc.callFunc(() => {
                            anim.play('anchor_attack');
                            anim.on('finished', () => {
                                let arrow = cc.instantiate(this.arrow)
                                this.canvas.addChild(arrow)

                                arrow.position = cc.v2(this.chArray[i].x, this.chArray[i].y);
                                let Action65 = cc.sequence(cc.moveBy(0.5, em.x - arrow.x, em.y - arrow.y), cc.callFunc(() => {
                                    if (em) {
                                        em.health -= 180 * mark
                                        if (em.health <= 0) {
                                            em.alive = false
                                            em.active = false
                                        }
                                    }
                                    arrow.destroy()
                                }));
                                arrow.runAction(Action65);
                                this.chArray[i].runAction(
                                    cc.moveBy(jumpTime, 0, -jumpY)
                                );
                                anim.play('anchor')
                                anim.off('finished');
                            });
                        });
                        let Action6 = cc.sequence(cc.moveBy(jumpTime, 0, jumpY), finished6);
                        this.chArray[i].runAction(Action6);
                        break;
                    case "magic1":
                        anim.play('magic_attack');
                        anim.on('finished', () => {
                            anim.play('magic')
                            anim.off('finished');
                        });

                        let themagic1 = cc.instantiate(this.firemagic)
                        this.canvas.addChild(themagic1)
                        themagic1.position = cc.v2(this.chArray[i].x, 200);

                        let Action7 = cc.sequence(cc.moveBy(1, em.x - themagic1.x, em.y - themagic1.y), cc.callFunc(() => {
                            if (em) {
                                em.health -= 70 * mark
                                if (em.health <= 0) {
                                    em.alive = false
                                    em.active = false
                                }
                            }
                            themagic1.destroy()
                        }));
                        themagic1.runAction(Action7);
                        break;

                    case "magic2":
                        anim.play('magic_attack');
                        anim.on('finished', () => {
                            anim.play('magic')
                            anim.off('finished');
                        });

                        let themagic2 = cc.instantiate(this.firemagic)
                        this.canvas.addChild(themagic2)
                        themagic2.position = cc.v2(this.chArray[i].x, 200);
                        let Action8 = cc.sequence(cc.moveBy(1, em.x - themagic2.x, em.y - themagic2.y), cc.callFunc(() => {
                            if (em) {
                                em.health -= 180 * mark
                                if (em.health <= 0) {
                                    em.alive = false
                                    em.active = false
                                }
                            }
                            themagic2.destroy()
                        }));
                        themagic2.runAction(Action8);
                        break;
                    default:
                        break;
                }
            }

        }

    },
    RunAway() {
        var rv = [];
        for (var i = 1; i <= 6; i++) {
            rv[i] = 200 + Math.random() * 200;
            this.chArray[i].runAction(cc.sequence(cc.moveBy(0.5, -1 * rv[i], 0), cc.moveBy(0.5, 0.1, 0), cc.moveBy(1, rv[i], 0)))

        }
    },

    attackselect(thech) {

    },
    readyAttack() {
        for (let i = 0; i < 3; i++) {
            if (this.enemy[i].x <= this.mainCamera.x + 738 / 2) {
                this.enemy[i].runAction(cc.sequence(cc.scaleBy(1, 1.5, 1.5), cc.scaleBy(1, 1 / 1.5, 1 / 1.5)))
            }
        }
    },
    monsterAttack() {
        for (let i = 0; i < 3; i++) {
            if (this.enemy[i].x <= this.mainCamera.x + 738 / 2 && this.enemy[i].active) {
                if (i == 0) {
                    this.attack1.position = cc.v2(this.enemy[i].x, this.enemy[i].y)
                    this.attack1.active = true
                } else if (i == 1) {
                    this.attack2.position = cc.v2(this.enemy[i].x, this.enemy[i].y + 300)
                    this.attack2.active = true
                    for (let a = 6; a > 1; a--) {
                        if (this.chArray[a].health > 0) {
                            this.attack2.runAction(cc.sequence(cc.moveTo(0.5, this.chArray[a].x, this.chArray[a].y), cc.callFunc(() => {
                                this.attack2.active = false
                            })))
                            break;
                        }
                    }
                } else if (i == 2) {
                    this.attack3.active = true
                    let anim = this.attack3.getComponent(cc.Animation);
                    if (this.Level == 1)
                        anim.play('attack3');
                    else
                        anim.play('attack2');
                }
            }
        }
    },
    detectAndDie(num) {
        this.chArray[num].alive = false
        let anim = this.chArray[num].getComponent(cc.Animation)
        anim.stop()
        let comp = this.chArray[num].getComponent(cc.Sprite)
        if (this.chList[num] == "knight11" || this.chList[num] == "knight12") {
            comp.spriteFrame = this.k1Die
        } else if (this.chList[num] == "knight21" || this.chList[num] == "knight22") {
            comp.spriteFrame = this.k2Die
        } else if (this.chList[num] == "anchor1" || this.chList[num] == "anchor2") {
            comp.spriteFrame = this.anchorDie
        } else if (this.chList[num] == "magic1" || this.chList[num] == "magic2") {
            comp.spriteFrame = this.magicDie
        }
    }


});