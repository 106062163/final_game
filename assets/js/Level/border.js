// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        target: 1,
        Level1_node: {
            default: null,
            type: cc.Node
        },
        Level2_node: {
            default: null,
            type: cc.Node
        },
        Level3_node: {
            default: null,
            type: cc.Node
        },
        Level4_node: {
            default: null,
            type: cc.Node
        },
        Level5_node: {
            default: null,
            type: cc.Node
        },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        this.Level1_node.on(cc.Node.EventType.MOUSE_ENTER, (event) => {
            this.borderMove(this.Level1_node)
        });
        this.Level1_node.on(cc.Node.EventType.MOUSE_LEAVE, (event) => {
            document.body.style.cursor = "default"
        });
        this.Level2_node.on(cc.Node.EventType.MOUSE_ENTER, (event) => {
            this.borderMove(this.Level2_node)
        });
        this.Level2_node.on(cc.Node.EventType.MOUSE_LEAVE, (event) => {
            document.body.style.cursor = "default"
        });
        this.Level3_node.on(cc.Node.EventType.MOUSE_ENTER, (event) => {
            this.borderMove(this.Level3_node)
        });
        this.Level3_node.on(cc.Node.EventType.MOUSE_LEAVE, (event) => {
            document.body.style.cursor = "default"
        });
        this.Level4_node.on(cc.Node.EventType.MOUSE_ENTER, (event) => {
            this.borderMove(this.Level4_node)
        });
        this.Level4_node.on(cc.Node.EventType.MOUSE_LEAVE, (event) => {
            document.body.style.cursor = "default"
        });
        this.Level5_node.on(cc.Node.EventType.MOUSE_ENTER, (event) => {
            this.borderMove(this.Level5_node)
        });
        this.Level5_node.on(cc.Node.EventType.MOUSE_LEAVE, (event) => {
            document.body.style.cursor = "default"
        });


    },
    update(dt) {

    },
    borderMove(targetNode) {
        document.body.style.cursor = "pointer"
        this.node.y = targetNode.y
    }
});