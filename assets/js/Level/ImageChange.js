// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        firstImage: cc.SpriteFrame,
        secondImage: cc.SpriteFrame,

        showTarget: Number
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        var sprite = this.getComponent(cc.Sprite);
        if (this.showTarget == 1) {
            sprite.spriteFrame = this.firstImage

        } else if (this.showTarget == 2) {
            sprite.spriteFrame = this.secondImage
        }
        this.node.on(cc.Node.EventType.MOUSE_UP, (event) => {
            switch (this.node.name) {
                case "Level1":
                    cc.director.loadScene("Level1");
                    break;
                case "Level2":
                    cc.director.loadScene("Level2");
                    break;
                case "Level3":
                    cc.director.loadScene("Level3");
                    break;
                case "Level4":
                    cc.director.loadScene("Level4");
                    break;
                default:
                    break;
            }
        });
    },

    // update (dt) {},
});